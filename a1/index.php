<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S05 Activity</title>
</head>
<body>

	<?php session_start(); ?>

	<h1>Login</h1>
	<form method="POST" action="./server.php">
		<!-- the value of the "action" attribute dictates where the form data should go -->
		
		Email: <input type="email" name="email" required>
		Password: <input type="text" name="password" required>
		<button type="submit">Login</button>
	</form>

	<?php if(isset($_SESSION['user'])): ?>
		<p>Hello, <?= $_SESSION['user']; ?></p>
	
	<?php else: ?>
		<?php if(isset($_SESSION['fail'])): ?>

					<p><?= $_SESSION['fail']; ?></p>

		<?php endif;?>	

	<?php endif ?>

	
</body>
</html>