<?php

session_start();

class TaskList{
	//method for adding a new task
	public function add($description){
		//create a variable named $newTask with the following information:
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		if($_SESSION['tasks'] === null){ //check if a session variable named tasks exists
			$_SESSION['tasks'] = array(); //if not, create a new one that is an empty array
		}

		array_push($_SESSION['tasks'], $newTask); //push the $newTask object to our session array
	}
	//method for updating an existing
	public function update($id, $description, $isFinished){
		$_SESSION['tasks'][$id]->description = $description; //change the description of the specific task to its new description
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false; //update the task to be marked as finished or not
	}

	public function remove($id){
		array_splice($_SESSION['tasks'], $id, 1);
	}
}

$taskList = new TaskList(); //create a new taskList object

if($_POST['action'] === 'add'){ //if the action input's value is add, call the add method
	$taskList->add($_POST['description']);
}else if($_POST['action'] === 'update'){ //if the action input's value is update, call the update method
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
}


header('Location: ./index.php'); //redirect the user back to index
